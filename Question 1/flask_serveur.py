from flask import Flask, json, request, Response

import numpy
import time
import sys
import os
import cv2
import threading

INPUT_FILE='zoo.mp4'

app = Flask(__name__)

@app.route("/helloWorld",methods=['GET'])
def helloWorld ():
    return "<h1>Hello world !</h1>"

@app.route("/getFrameMJpeg",methods=['GET'])
def getFrameFormatEnter():
    def gather_img():
        vs = cv2.VideoCapture(INPUT_FILE)
        while True :
            print("ICI")
            
            try:
                (grabbed, frameData) = vs.read()
                #cv2.imshow("output",frameData)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    break

                #frame = numpy.fromstring (frameData,dtype=numpy.uint8)
                #frame = frame.reshape (480,642,3)
                success, encoded_frame= cv2.imencode('.jpg', frameData)
                yield (b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + encoded_frame.tobytes() + b'\r\n')
            except:
                break
        #return getFrameFormat()
    return Response(gather_img(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=5000, host='127.0.0.1')