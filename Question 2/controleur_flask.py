from flask import Flask, json, request, Response

import numpy
import time
import sys
import os
import cv2
import threading
from serviceTraitement import *
import numpy as np
import time
import cv2



app = Flask(__name__)

@app.route("/helloWorld",methods=['GET'])
def helloWorld ():
    return "<h1>Hello world !</h1>"

@app.route("/getFrameMJpeg",methods=['GET'])
def getFrameFormatEnter():
    return Response(executeIA_fonct(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route("/getDataFrame",methods=['GET'])
def getFrameFormatTxtEnter():
    return Response(executeIA_fonct_txt(), mimetype='application/json')


if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=5000, host='127.0.0.2')


